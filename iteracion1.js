'use strict';

//Iteracion #1: Buscar el máximo.
function sum (numberOne, numberTwo) {
    if (numberOne > numberTwo){
        console.log(numberOne);
    } else {
        console.log(numberTwo);
    }
}
//Iteracion #2: Buscar la palabra más larga.
const avengers = ['Hulk', 'Thor', 'IronMan', 'Captain A.', 'Spiderman', 'Captain M.'];
function findLongestWord(listString) {
  let longest = '';
  //for (let index = 0; index < listString.length; index++) {
    //const current = listString[index]; // En cada vuelta le asigna uno de los valores del array
    //if(current.length > longest.length){// Va comparando con lo que haya guardado en longest, si es mayor lo guarda, si no continua.
    //  longest = current;
   // }
  // }
/*
for (const item of listString) { //Ejemplo con for of
  if(item.length > longest.length){
    longest = item;
  }
}
*/
listString.forEach(item => {
  if(item.length > longest.length){
    longest = item;
  }
});
   return longest;
}
const result2 = findLongestWord(avengers);
console.log('El más largo es: ', result2);

//Iteración #3: Calcular la suma.

const numberList = [1, 2, 3, 5, 45, 37, 58];

function sumAll(list) { //Funcion sumAll
  let sum = 0; //Inicializamos en 0 
for (const value of list) { //el bucle for of va recorriendo los numeros y añadiendolos a value
  sum = sum + value; //el primer valor se asigna a value, se reproduce el bucle y asigna el segundo valor a value que lo suma a sum
}
 return sum;
}
const result3 = sumAll(numberList);
console.log('La suma es: ', result3);
//Iteracion #4: Calcular el promedio
const numbers = [12, 21, 38, 5, 45, 37, 6];
const long = numbers.length;

function average(list) {
  let sum = 0;
  for (const value of list) {
    sum = sum + value;
  }
  return sum;
}

const result4 = average(numbers)/long;
console.log(result4);

//Iteracion #5: Calcular promedio de strings
//Crea una función que reciba por parámetro un array y cuando es un valor number lo sume y 
//de lo contrario cuente la longitud del string y lo sume. Puedes usar este array para probar tu función:

const mixedElements = [6, 1, 'Rayo', 1, 'vallecano', '10', 'upgrade', 8, 'hub'];
function averageWord(list) {
  let acc = 0;
    for (const value of list) {
    if(typeof value === 'number'){
      acc += value; //Si es number lo suma
    } else if (typeof value === 'string'){
      acc += value.length;// Aqui cuenta la longitud del string y lo suma
    }
    
  }
  return acc / list.length;
}

const result5 = averageWord(mixedElements);
console.log('Media de result5: ', result5);

//Iteración #6: Valores únicos.
/*
Crea una función que reciba por parámetro un array y compruebe si existen elementos duplicados, 
en caso que existan los elimina para retornar un array sin los elementos duplicados. Puedes usar este array para probar tu función:
*/
const foodList = [
  'sushi',
  'pizza',
  'burger',
  'potatoe',
  'pasta',
  'ice-cream',
  'pizza',
  'chicken',
  'onion rings',
  'pasta',
  'soda'
];
function removeDuplicates(list) {
      let finalList = []; //Es el array que devuelve el return

      list.forEach(item => {
        if(!finalList.includes(item)){ //Voy recorriendo el array, si finalList NO incluye el elemento item, uso push para subirlo.
          finalList.push(item);
        }
      });
      return finalList;
  };

  const result6 = removeDuplicates(foodList);
  console.log('Result6: ', result6);

  //Iteración #7: Buscador de nombres
  /*
  Crea una función que reciba por parámetro un array y el valor que desea comprobar que existe dentro de dicho array - comprueba si existe 
  el elemento, en caso que existan nos devuelve un true y la posición de dicho elemento y por la contra un false. Puedes usar este array para probar tu función:
  */
  const nameList = [
    'Peter',
    'Steve',
    'Tony',
    'Natasha',
    'Clint',
    'Logan',
    'Xabier',
    'Bruce',
    'Peggy',
    'Jessica',
    'Marc'
  ];
  function finderName(list, match) {
    let result = {
      exist: false,
      position: -1
    };
    
    list.forEach((item, index) => {
      if(item === match) {
        result = {
          exist: true,
          position: index
        };
      }
    })
    return result;
  }
  const result7 = finderName(nameList, 'Peggy');
  console.log('Result7: ', result7);

//Iteracion #8: Contador de repeticiones
/*
Crea una función que nos devuelva el número de veces que se repite cada una de las palabras que lo conforma.  
Puedes usar este array para probar tu función:
*/
const wordList = [
  'code',
  'repeat',
  'eat',
  'sleep',
  'code',
  'enjoy',
  'sleep',
  'code',
  'enjoy',
  'upgrade',
  'code'
];
function repeatCounter(list) {
  const result = {
    code: 4,
    repeat: 1,
  };

  //Paso 1 Iterar el listado
for(let i = 0; i < list.length; i++) {
  const item = list[i];

  //Paso 2 Si la palabra no existe tengo que crearla y ponerle un 1
  if (!result[item]) {  //En este paso comprobamos si en result existe la palabra
    result[item] = 1;
  } else {
    result[item] += 1;
  }

}


  //Si si existe tengo que incrementarle el contador
}

const result8 = repeatCounter(wordList);
console.log('Result8: ', result8)